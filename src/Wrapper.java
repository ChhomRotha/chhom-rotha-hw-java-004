import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

public class Wrapper<E> {
    private ArrayList<E> list = new ArrayList<>();

    public void addItem(E value) throws DuplicatedExecption, NullExecption {
        if(list.contains(value)){
            throw new DuplicatedExecption("Duplicated Value : " + value);
        }
        else if (value == null){
            throw new NullExecption("Input null value");
        }
        else
        list.add(value);

    }
    public E getItem(int index){
        return list.get(index);
    }
    public int listLength(){
        return this.list.size();
    }

}
