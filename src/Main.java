public class Main {

    public static void main(String[] args) {
        Wrapper<Integer> integerWrapper = new Wrapper<>();
        try {
            integerWrapper.addItem(1);
//            integerWrapper.addItem(null);
//            integerWrapper.addItem(1);
        }catch (Exception e) {
            System.out.println(e);
        }

        System.out.println("\n List all from input");
        for (int i=0; i<integerWrapper.listLength(); i++){
            System.out.println(integerWrapper.getItem(i));
        }
    }
}
